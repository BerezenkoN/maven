package Bank;

import Bank.service.AccountService;
import Bank.service.CustomerService;
import Bank.service.TransactionService;

/**
 * Created by user on 10.12.2016.
 */
public class Main {
    public static void main(String[] args) {
        AccountService accountService = new AccountService();
        CustomerService customerService = new CustomerService();
        TransactionService transactionService = new TransactionService();
        System.out.println(accountService.list());
        System.out.println(customerService.list());
        System.out.println(transactionService.list());
    }
}
