package Bank.service;

import Bank.dao.CustomerDao;
import Bank.dao.CustomerDaoImp;
import Bank.domain.Account;
import Bank.domain.Customer;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by user on 01.12.2016.
 */
public class CustomerService {
    private CustomerDao cDao = new CustomerDaoImp();
    private AccountService aSevice = new AccountService();

    public Customer get (Long customerId){
        return  cDao.get(customerId);

    }
    public Long save (Customer customer) throws SQLException {
        Long customerId = cDao.save(customer);
        aSevice.save(new Account(null, BigDecimal.ZERO, Timestamp.valueOf(java.time.LocalDateTime.now()), "UAH", false, customerId));
        return customerId;
    }
    public  void update (Customer customer) throws SQLException {
        cDao.update(customer);
    }
    public void delete (Customer customer) throws SQLException {
        aSevice.deleteByCustomer(customer.getId());
        cDao.delete(customer);
    }
    public List<Customer> list() {

               return cDao.list();

    }



}
