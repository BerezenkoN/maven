package Bank.service;

import Bank.dao.TransactionDao;
import Bank.dao.TransactionDaoImp;
import Bank.domain.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 30.11.2016.
 */
public class TransactionService {
    private TransactionDao tDao = new TransactionDaoImp();

    public Transaction get(Long id){
       return tDao.get(id);
    }

    void deleteByAccount(Long accountnumber) throws SQLException {
        tDao.deleteByAccount(accountnumber);
    }

    public Long save(Transaction transaction) throws SQLException {
        return tDao.save(transaction);

    }
    public  void update(Transaction transaction) throws SQLException {
        tDao.update(transaction);
    }
    public void delete(Transaction transaction) throws SQLException {
        tDao.delete(transaction);
    }
    public List<Transaction> list(){
        return tDao.list();
    }

}

